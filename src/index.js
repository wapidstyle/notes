//
// Notes: free, online notes denoted by a UUID, along with an extension.
//
// This code is under the BSD-3-Clause license.
//
import {default as http} from "http";
import {default as express} from "express";
import {default as winston} from "winston";
import {default as mongoose} from "mongoose";
import {safeLoad as yaml} from "js-yaml";
import {readFile as _readFile} from "graceful-fs";
import {promisify} from "bluebird";
import {default as minimist} from "minimist";
import {default as _} from "lodash";
import {default as deepForEach} from "deep-for-each";
import {default as notevil} from "notevil";
import {default as exwinston} from "express-winston";
import {getClientIp as requestIp} from "request-ip";
import {default as _io} from "socket.io";
import {default as bodyParser} from "body-parser";

import {default as controllers} from "./controllers";
import {default as socketControllers} from "./socket-controllers";

const args = minimist(process.argv.slice(2));
const readFile = promisify(_readFile);
const configPath = require("path").join(__dirname, "config.yml");

let io;
let mainConfig;
let httpServer;

winston.loggers.add("main", {
	console: {
		level: args.verbose ? "verbose" : "info",
		colorize: true,
		label: "main"
	}
});
const logger = winston.loggers.get("main");

// HACK Trick XO into fixing lint error
export default null;

logger.info("Checking command-line args compared to config...");

readFile(configPath)
	.then(yaml)
	.then((config) => {
		if (config.default) {
			logger.warn("You appear to be using the default config, you might want " +
				"to change that.");
		}
		// Process the data to match args
		deepForEach(config, (element, prop, subject, path) => {
			if (!_.isObject(element)) {
				const returnedElement = _.get(args, path) || notevil(element.toString());
				_.set(config, path, returnedElement);
				logger.verbose("config: " + path + ": " + returnedElement);
			}
		});
		return config;
	}).then((config) => {
		mainConfig = config;

		logger.info("Formulating MongoDB connection address and passing to Mongo...");
		return "mongodb://" + config.mongodb.user + ":" + process.env[config.mongodb.pass] +
			"@" + config.mongodb.host + ":" + config.mongodb.port + "/" + config.mongodb.db;
	}).then((uri) => {
		// TODO Use this direct in the promise w/out a wrapper function
		mongoose.connect(uri);
	});

mongoose.connection.on("open", () => {
	logger.info("Connected to MongoDB, loading server");

	logger.verbose("Initializing Express instance");
	const app = express();

	app.use((req, res, next) => {
		// HACK We can't set req.ip, use res instead
		res.ip = requestIp(req);
		next();
	});

	logger.verbose("Initializing Express logger");
	winston.loggers.add("request", {
		console: {
			level: args.verbose ? "verbose" : "info",
			colorize: true,
			label: "request"
		}
	});
	app.use(exwinston.logger({
		transports: [
			new winston.transports.Console({
				level: args.verbose ? "verbose" : "info",
				colorize: true,
				label: "request"
			})
		],
		winstonInstance: winston.loggers.get("request"),
		msg: "Request from {{res.ip}}: {{req.method}} {{req.url}}",
		colorize: true,
		meta: false
	}));

	app.use(bodyParser.urlencoded({
		limit: "1mb",
		extended: false
	}));

	logger.verbose("Initializing controllers");
	app.get(mainConfig.http.prefix, controllers.main);
	app.get(mainConfig.http.prefix + "js/*", controllers.js);
	app.get(mainConfig.http.prefix + "css/*", controllers.css);
	app.get(mainConfig.http.prefix + "*", controllers.note);
	app.post(mainConfig.http.prefix + "*", controllers.postNote);

	logger.verbose("Initializing HTTP server");
	httpServer = new http.Server(app);
	io = _io(httpServer);

	logger.verbose("Initializing Socket.io");
	io.on("connection", socketControllers);

	logger.verbose("Starting server");
	const address = (typeof mainConfig.http.host === "undefined" ? "" : mainConfig
		.http.host + ":") + mainConfig.http.port;
	httpServer.listen(address, () => {
		logger.info("Listening on " + address);
	});
});

for (;;) {
	if (mainConfig !== {}) {
		break;
	}
}
const _mainConfig = mainConfig;
export {_mainConfig as mainConfig};
