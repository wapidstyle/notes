//
// Notes: free, online notes denoted by a UUID, along with an extension.
//
// This code is under the MIT license.
//
import {default as minimist} from "minimist";
import {default as winston} from "winston";

import {Noteboard} from "./controllers";

winston.loggers.add("sockets", {
	console: {
		level: minimist(process.argv.slice(2)).verbose ? "verbose" : "info",
		colorize: true,
		label: "sockets"
	}
});
const socketLogger = winston.loggers.get("sockets");

export const socketControllers = (socket) => {
	socket.on("update", (data) => {
		socketLogger.info("Socket connection from " + socket.request.connection.remoteAddress);
		Noteboard.findOne({uuid: data.uuid})
			.then((noteboard) => {
				noteboard.notes = data.notes;
				noteboard.save();
			});
	});
};

export default socketControllers;
