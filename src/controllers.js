//
// Notes: free, online notes denoted by a UUID and an extension.
//
// This code is under the BSD-3-Clause license.
//
import {default as hat} from "hat";
import {default as mongoose} from "mongoose";
import {default as readFile} from "read-cache";
import promisify from "bluebird";
import {default as handlebars} from "handlebars";
import {default as _} from "lodash";
import {default as winston} from "winston";
import {default as uglify} from "uglify-js";
import {default as flatCache} from "flat-cache";
import {getClientIp as requestIp} from "request-ip";

import {default as socketControllers} from "./socket-controllers";
import {default as mainConfig} from "./index";

// We're not persisting, use hat to prevent overlap just in case
const cache = flatCache.load(hat());

const Noteboard = mongoose.model("Noteboard", new mongoose.Schema({
	uuid: String,
	notes: String
}));

export {Noteboard};

/* eslint no-unused-vars:off */
let controllers;

export default controllers = {
	main: (req, res) => {
		// They didn't gib a UUID! let's make one
		const sequence = hat(256);
		const uuidObject = [
			sequence.split("").slice(0, 9).join(""),
			sequence.split("").slice(9, 13).join(""),
			sequence.split("").slice(13, 17).join(""),
			sequence.split("").slice(17, 21).join(""),
			sequence.split("").slice(21, 33).join("")
		];
		res.redirect(mainConfig.http.prefix + uuidObject.join("-"));
		res.end();
	},
	note: (req, res) => {
		// They gib a UUID! If it exists in Mongo, open it. Otherwise, make it.
		if (new RegExp("^" + mainConfig.http.prefix + ".........-....-....-....-............$").test(req.url)) {
			Noteboard.findOne({uuid: req.url.split(mainConfig.http.prefix).join("")}).then((noteboard) => {
				if (_.isNull(noteboard)) {
					winston.loggers.get("request").warn("Notebook for " + req.url
						.split(mainConfig.http.prefix).join("") + "is nonexistant, making it.");
					noteboard = new Noteboard({
						uuid: req.url.split(mainConfig.http.prefix).join(""),
						notes: []
					});
					noteboard.save();
				}

				readFile("src/index.html").then((contents) => {
					return handlebars.compile(_.toString(contents));
				}).then((template) => {
					res.end(template({
						code: req.url.split(mainConfig.http.prefix).join(""),
						notes: noteboard.notes,
						prefix: mainConfig.http.prefix
					}));
				});
			});
		} else {
			res.redirect(mainConfig.http.prefix);
			res.end();
		}
	},
	js: (req, res) => {
		const fileName = req.url.split("/").slice(mainConfig.http.prefix.split("/").length + 1, req.url.split("/")
			.length).join("/");
		readFile("node_modules/" + fileName, "utf-8").then((contents) => {
			const fileType = req.url.split(".")[req.url.split(".").length - 1];

			if (fileType === "js" && _.isUndefined(cache.getKey(fileName))) {
					/* eslint camelcase:off */
				contents = uglify.minify(contents, {
					compress: {
						dead_code: true,
						global_defs: {
							DEBUG: false
						}
					}
				}).code;
				cache.setKey(fileName, contents);
			} else if (fileType === "js" && !_.isUndefined(cache.getKey(fileName))) {
				contents = cache.getKey(fileName);
			}

			res.type(fileType).end(contents);
		}).catch((err) => {
			if (err.code === "ENOENT") {
				res.status(404);
				res.end("Error 404: Not Found");
			} else {
				winston.loggers.get("request").error("Request for " + req.url + " failed: " + err.message);
				winston.loggers.get("request").error(err.stack);
				res.status(500);
				res.end("Error 500: Internal Server Error");
			}
		});
	},
	css: (req, res) => {
		readFile("static/" + req.url.split("/").slice(2, req.url.split("/")
			.length).join("/"), "utf-8").then((contents) => {
				res.end(contents);
			}).catch((err) => {
				if (err.code === "ENOENT") {
					res.status(404);
					res.end("Error 404: Not Found");
				} else {
					winston.loggers.get("request").error("Request for " + req.url + " failed: " + err.message);
					winston.loggers.get("request").error(err.stack);
					res.status(500);
					res.end("Error 500: Internal Server Error");
				}
			});
	},
	postNote: (req, res) => {
		/*
		 * HACK We will cheat a bit by giving a fake EventEmitter to the Socket.io,
		 * and then immediately call "update" with the text.
		 */
		const data = req.body.data;

		socketControllers({
			request: {
				connection: {
					remoteAddress: requestIp(req)
				}
			},
			on: (name, func) => {
				// Futureproof
				if (name === "update") {
					func({
						notes: data,
						uuid: req.url.split("/")[req.url.split("/").length - 1]
					});
				}
			}
		});
		res.redirect(req.url);
		res.end();
	}
};
