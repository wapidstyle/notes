# `notes`
A simple online note editor.

## Indexing and Security
**There is currently no security - anyone can edit your note.**

However, they URLs are *designed* to be secure-ish. Look at
this one:
```
localhost:8080/38a7a3f0c-f590-0fd5-a94b-01eedcdc62f8
```
It would be pretty hard to guess that by bruteforce.

## Usage
```sh
# First, clone and install. If you don't have Yarn, get
# it - it's cool.
git clone https://bitbucket.org/wapidstyle/notes.git
yarn # or "npm install"

# IF YOU HAVE NPM: replace the "yarn" with "npm run".
# Running, default mode
yarn run
# With --verbose
yarn run:verbose
# Build JS but don't run
yarn build
# Lint
yarn lint
# Check for TODO, HACK and FIXME
yarn comments
```
