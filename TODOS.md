# TODOs
1. Add some testing, preferably with [AVA](https://github.com/avajs/ava). 
2. Make it more RESTful.
3. Add Passport functionality for saving your UUIDs.  
   i.e. I could log in with wapidstyle@live.ca, and it would give me a list of
   UUIDs I could choose from as my notes.
